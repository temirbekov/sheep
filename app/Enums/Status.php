<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Status extends Enum
{
    const DELETED = -1;
    const UNPUBLISHED = 0;
    const PUBLISHED = 1;
}
