<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    CONST ACTION_KILL = 1;
    CONST ACTION_MOVE = 2;
    CONST ACTION_BORN = 3;

    protected $fillable = [
        'sheep_name', 'action_type', 'sheep_id', 'yard_id', 'day_num'
    ];
}
