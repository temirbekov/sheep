<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayStat extends Model
{
    protected $fillable = [
        'day_num', 'farm_id', 'count_sheep', 'killed_sheep', 'live_sheep', 'max_count_yard', 'min_count_yard'
    ];
}
