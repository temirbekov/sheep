<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $fillable = [
        'name'
    ];

    public function yards()
    {
        return $this->hasMany('App\Yard');
    }

    public function lastDayNum()
    {
        return $this->hasOne('App\DayStat')->max('day_num');;
    }
}
