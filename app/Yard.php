<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yard extends Model
{
    protected $fillable = [
        'name'
    ];

    public function sheep()
    {
        return $this->hasMany('App\Sheep');
    }
}
