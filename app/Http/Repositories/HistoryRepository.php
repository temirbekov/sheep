<?php

namespace App\Repositories;

use App\History;
use Illuminate\Support\Collection;

class HistoryRepository
{
    protected $model;
    protected $history;

    public function __construct(History $model)
    {
        $this->model = $model;
        $this->history = Collection::make();
    }

    public function newAction(\App\Sheep $sheep, $action_type, $dayNum, $yard_id, $old_yard_id=null) {
        $data = [
            'sheep_name'    => $sheep->name,
            'sheep_id'      => $sheep->id,
            'yard_id'       => $yard_id,
            'old_yard_id'   => $old_yard_id,
            'action_type'   => $action_type,
            'day_num'       => $dayNum
        ];
        $this->history->push($data);
        return History::create($data);
    }

    public function getCurrentHistory() {
        return $this->history;
    }

}
