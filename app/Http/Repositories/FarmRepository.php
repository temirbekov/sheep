<?php

namespace App\Repositories;


use App\Farm;
use App\Sheep;
use App\Yard;
use Illuminate\Support\Facades\DB;

class FarmRepository
{
    protected $model;

    public function __construct(Farm $model)
    {
        $this->model = $model;
    }

    public function getYards($farm_id) {
        return collect(DB::select('
            SELECT y.id as yard_id, COUNT(m.id) AS count_sheep
            FROM `yards` AS y
            LEFT JOIN sheep AS m ON y.id = yard_id
            WHERE y.farm_id = :farm_id
            GROUP BY y.id
          ', ['farm_id' => $farm_id]
        ));
    }

    public function killSheep(\App\Sheep $sheep) {
        return $sheep->delete();
    }

    public function moveSheep(\App\Sheep $sheep, int $newYardID) {
        $sheep->yard_id = $newYardID;
        return $sheep->save();
    }

    public function createFarm() {
        $farm = factory(Farm::class)->create();

        //Добавим каждой четыре загона
        for ($i = 1; $i <= 4; $i++) {
            $yard = new Yard();
            $yard->name = 'Yard '.$i;
            $farm->yards()->save($yard);
        }

        //Заселим овечек в случайном порядке
        for ($s = 1; $s <= 10; $s++) {
            $sheep = factory(Sheep::class)->make();
            $farm->yards->random()->sheep()->save($sheep);
        }
        return $farm;
    }
}
