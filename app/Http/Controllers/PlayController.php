<?php

namespace App\Http\Controllers;

use App\DayStat;
use App\Farm;
use App\History;
use App\Http\Resources\FarmResource;
use App\Http\Resources\FarmResourceShort;
use App\Repositories\FarmRepository;
use App\Repositories\HistoryRepository;
use App\Sheep;

class PlayController extends ApiBaseController
{
    CONST DAY_INTERVAL = 3;

    protected $farmRepository;

    public function __construct(FarmRepository $farmRepository)
    {
        $this->farmRepository = $farmRepository;
    }

    public function index()
    {
        return view('home');
    }

    public function create() {
        return $this->sendResponse(
            true, FarmResourceShort::make($this->farmRepository->createfarm())
        );
    }

    public function list() {
        $farms = Farm::with(['yards','yards.sheep'])->get();

        return $this->sendResponse(
            true, FarmResourceShort::collection($farms)
        );
    }

    public function farm($id) {
        $farm = Farm::with(['yards','yards.sheep'])->findOrFail($id);

        return $this->sendResponse(
            true, FarmResource::make($farm)
        );
    }

    public function tick($id, FarmRepository $farmRepository, HistoryRepository $historyRepository) {
        $farm = Farm::findOrFail($id); //WITH Добавить

        //Получаем все загоны с кол-во овечек
        $yards = $farmRepository->getYards($farm->id);

        //Коллекция загонов где больше одной овечки
        $moreThanOne = $yards->filter(function ($value) {
            return $value->count_sheep > 1;
        });

        //Получаем последний день
        $last_day = $farm->lastDayNum();
        $current_day = $last_day + 1;

        if($moreThanOne->isNotEmpty()) {

            //каждый 10 день одну любую овечку забирают
            //загоны никогда не должны быть пусты, значит можем забирать только из загонов где больше одной
            if($last_day && ($last_day % self::DAY_INTERVAL == 0)) {

                //Получаем случайный загон
                $rand_yard = $moreThanOne->random();

                //Забираем случайную овечку
                $sheep = Sheep::where('yard_id',$rand_yard->yard_id)->inRandomOrder()->first();

                //Убиваем
                if($farmRepository->killSheep($sheep)) {
                    $historyRepository->newAction($sheep, History::ACTION_KILL, $current_day, $sheep->yard_id);
                    //Обновляем коллекцию со счетчиком
                    $rand_yard->count_sheep--;
                }
            }
        }

        //Рождается новая овечка

        //Берем новый случайный загон для рождения овечки
        $born_yard = $yards->random();

        //Создаем овечку
        $new_sheep = factory(Sheep::class)->make();
        //Помещаем в загон
        if ($farmRepository->moveSheep($new_sheep, $born_yard->yard_id)) {
            $born_yard->count_sheep++;
            $historyRepository->newAction($new_sheep, History::ACTION_BORN, $current_day, $born_yard->yard_id);
        }

        //Загон с где осталась одна овечка или меньше
        $yardLessOne = $yards->first(function ($value, $key) {
            return $value->count_sheep <= 1;
        });

        if($yardLessOne) {
            $max_sheep = $yards->max('count_sheep');
            //Загон с максмальным кол-вом овечек
            $yard_max = $yards->first(function ($value, $key) use ($max_sheep) {
                return $value->count_sheep == $max_sheep;
            });

            //Забираем случайную овечку
            $sheepToMove = Sheep::where('yard_id',$yard_max->yard_id)->inRandomOrder()->first();

            if($farmRepository->moveSheep($sheepToMove, $yardLessOne->yard_id)) {
                $yardLessOne->count_sheep++;
                $yard_max->count_sheep--;
                $historyRepository->newAction($sheepToMove, History::ACTION_MOVE, $current_day, $yardLessOne->yard_id, $yard_max->yard_id);
            }
        }

        DayStat::create([
            'day_num'       => $last_day + 1,
            'farm_id'       => $farm->id,
            'count_sheep'   => $yards->sum('count_sheep'),
            'killed_sheep'  => 0,
            'live_sheep'    => 0,
            'max_count_yard'=> $yards->max('count_sheep'),
            'min_count_yard'=> $yards->min('count_sheep')
        ]);


        return $this->sendResponse(
            true, ['last_day'=>$current_day,'history'=>$historyRepository->getCurrentHistory()]
        );
    }
}
