<?php

use Faker\Generator as Faker;

$factory->define(App\Sheep::class, function (Faker $faker) {
    return [
        'name' => $faker->name.' '.$faker->colorName,
    ];
});
