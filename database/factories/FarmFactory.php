<?php

use Faker\Generator as Faker;

$factory->define(App\Farm::class, function (Faker $faker) {
    return [
        'name' => $faker->city.' '.$faker->company,
    ];
});
