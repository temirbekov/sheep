<?php

use App\Repositories\FarmRepository;
use Illuminate\Database\Seeder;

class FarmSeeder extends Seeder
{

    public function run(FarmRepository $farmRepository)
    {
        //Создадим сразу три фермы
        for ($f = 1; $f <= 3; $f++) {
            $farmRepository->createFarm();
        }
    }
}
