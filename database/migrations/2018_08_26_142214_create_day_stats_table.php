<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_stats', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('day_num');

            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')
                ->on('farms')->onDelete('cascade');

            $table->unsignedInteger('count_sheep');
            $table->unsignedInteger('killed_sheep');
            $table->unsignedInteger('live_sheep');
            $table->unsignedInteger('max_count_yard');
            $table->unsignedInteger('min_count_yard');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_stats');
    }
}
