import {HTTP} from './http-common'
import { EventBus } from '../event-bus.js';

const ACTION_KILL = 1;
const ACTION_MOVE = 2;
const ACTION_BORN = 3;

const ACTION_LABEL = [
    {
        id: ACTION_KILL,
        text: 'Забрали'
    },
    {
        id: ACTION_MOVE,
        text: 'Переместили'
    },
    {
        id: ACTION_BORN,
        text: 'Родилась'
    }
]

export default {
    namespaced: true,
    state: {
        farm: null,
        items: [],
        list: [],
        loading: false,
        day: null
    },
    mutations: {
        addSheep(state, payload) {
            let i = 0
            for (i = 0; i < state.items.length; i++) {
                if (state.items[i].id == payload.yard_id) {
                    state.items[i].sheep.push({
                        id: payload.sheep_id,
                        name: payload.sheep_name,
                        yard_id: payload.yard_id,
                    })
                }
            }
        },
        killSheep(state, payload) {
            let i = 0
            for (i = 0; i < state.items.length; i++) {
                let s = 0
                for (s = 0; s < state.items[i].sheep.length; s++) {
                    if (state.items[i].sheep[s].id == payload.sheep_id) {
                        state.items[i].sheep.splice(s, 1)
                    }
                }
            }
        },
        setLoading(state, payload) {
            state.loading = payload
        },
        setFarm(state, payload) {
            state.farm = payload
        },
        setDay(state, payload) {
            state.day = payload
        },
        setItems(state, payload) {
            state.items = payload
        },
        setList(state, payload) {
            state.list = payload
        },
    },
    actions: {
        async createFarm({commit}) {
            try {
                commit('setLoading', true)
                let response = await HTTP.post(`/play/create`)

                if (response.data.success === true) {
                    this.dispatch('farm/list')
                } else {
                    commit('setLoading', false)
                    throw new Error('Bad response')
                }

            } catch (e) {

            }
        },
        async clearItems({commit}) {
            commit('setFarm', null)
            commit('setItems', null)
        },
        async list({commit}) {
            commit('setLoading', true)
            try {
                let response = await HTTP.get(`/play/list`)

                commit('setLoading', false)
                if (response.data.success === true) {
                    commit('setList', response.data.result)
                    return response.data.result
                } else {
                    throw new Error('Bad response')
                }
            } catch (e) {

            }
        },
        async getAll({commit}, id) {
            try {
                commit('setLoading', true)
                let response = await HTTP.get(`/play/farm/${id}`)

                if (response.data.success === true) {
                    commit('setFarm', response.data.result.name)
                    commit('setDay', response.data.result.last_day)
                    commit('setItems', response.data.result.yards)
                    commit('setLoading', false)
                    return response.data.result
                } else {
                    throw new Error('Bad response')
                }
            } catch (e) {
                commit('setLoading', false)
                throw e
            }
        },
        async tick({commit}, id) {
            try {
                let response = await HTTP.get(`/play/tick/${id}`)

                if (response.data.success === true) {
                    commit('setDay', response.data.result.last_day)

                    let textError = ''

                    response.data.result.history.forEach(function (message) {
                        switch (message.action_type) {
                            case ACTION_KILL:
                                commit('killSheep', message)
                                break;
                            case ACTION_BORN:
                                commit('addSheep', message)
                                break;
                            case ACTION_MOVE:
                                commit('killSheep', message)
                                commit('addSheep', message)
                                break;
                            default:
                        }
                    });


                    let errors = response.data.result.history
                    for (var error in errors) {



                        textError = textError + '<li>' + errors[error].sheep_name + ' - <b>'+ ACTION_LABEL.find(item => item.id === errors[error].action_type).text + '</b></li>';
                    }
                    EventBus.$emit('message', '<ul>' + textError + '</ul>');

                    return response.data.result
                } else {
                    throw new Error('Bad response')
                }
            } catch (e) {
                throw e
            }
        },
    },
    getters: {
        loading(state) {
            return state.loading
        },
        items(state) {
            return state.items
        },
        farm(state) {
            return state.farm
        },
        list(state) {
            return state.list
        },
        day(state) {
            return state.day
        },
    }
}
