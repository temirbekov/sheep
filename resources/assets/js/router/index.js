import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/Home'
import Farm from '../pages/Farm'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/farm/:id',
            name: 'farm',
            component: Farm,
        }
    ]
})
