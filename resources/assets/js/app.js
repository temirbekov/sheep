
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

import App from './components/App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import _ from 'lodash'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
window.Vue = require('vue')

Vue.use(Vuetify)
Object.defineProperty(Vue.prototype, '$_', { value: _ })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  router,
  store,
  el: '#app',
  components: { App },
  template: '<App/>',
})
