<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/play/farm/{id}', 'PlayController@farm')->name('play.farm');
Route::get('/play/tick/{id}', 'PlayController@tick')->name('play.tick');
Route::get('/play/list', 'PlayController@list')->name('play.list');

Route::post('/play/create', 'PlayController@create')->name('play.create');

Route::get('/{any}', 'PlayController@index')->where('any', '.*');
